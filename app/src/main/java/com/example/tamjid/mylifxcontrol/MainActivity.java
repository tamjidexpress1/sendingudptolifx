package com.example.tamjid.mylifxcontrol;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import java.io.IOException;

import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import java.net.InetAddress;

import java.net.UnknownHostException;



public class MainActivity extends AppCompatActivity {

    String allOn="2a:00:00:34:b4:3c:f0:84:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01:0d:00:00:00:00:00:00:00:00:75:00:00:00:ff:ff:e8:03:00:00";
    String allOff="2a:00:00:34:b4:3c:f0:84:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01:0d:00:00:00:00:00:00:00:00:75:00:00:00:00:00:e8:03:00:00";
    int serverPort=56700;
    String asCii="*\0\04\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0u\0\0\0ÿÿ^C\0\0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button onbut= (Button) findViewById(R.id.onbutton);
        Button offbut= (Button) findViewById(R.id.offbutton);

        StringBuilder sb=new StringBuilder();
        try {

            InetAddress broadCast=InetAddress.getByName("255.255.255.255");

            byte ao[]=byteBuilder(allOn);
            byte aof[]=byteBuilder(allOff);


if (ao!=null&& aof!=null)

            { final DatagramPacket allOnPack=new DatagramPacket(ao,42,broadCast,serverPort);
            final DatagramPacket allOffPack=new DatagramPacket(aof,42,broadCast,serverPort);
                onbut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        backTask on=new backTask();
                        on.execute(allOnPack);

                    }
                });
                offbut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        backTask off=new backTask();
                        off.execute(allOffPack);

                    }
                });}




        }  catch (UnknownHostException e) {
            e.printStackTrace();
        }
        /*catch(IllegalArgumentException e){
            e.printStackTrace();
        }*/




    }

    public byte[] byteBuilder(String hex){
        String command=hex;
        byte byt_cmd[]=new byte[42];
        char[] byt=new char[2];
        for(int i=0,j=0,k=0;i<command.length();i++){
            char c=command.charAt(i);
            if(c==':'){
                j=0;
               BigInteger big=new BigInteger(String.valueOf(byt),16);
                byte x=(byte) Integer.parseInt(String.valueOf(big));
                byt_cmd[k++]=x;
            }
            else{
                byt[j]=c;
                j++;
            }
        }
        return byt_cmd;
    }

   //public static String hexToBin(String s) {
        //return new BigInteger(s, 16).toString(2);}

    private class backTask extends AsyncTask<DatagramPacket,Void,Void>{

        @Override
        protected Void doInBackground(DatagramPacket... params) {
            try {
                final DatagramSocket s= new DatagramSocket();
                s.send(params[0]);

                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        return null;}
    }

}
